import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ChoicesComponent } from './components/choices/choices.component';
import { PlatformLoginDialog } from './forms/platform-login/platform-login.dialog';
import { PlatformUserRegisisterDialog } from './forms/platform-user-regisister/platform-user-regisister.dialog';
import { SellerDashboardComponent } from './pages/seller-dashboard/seller-dashboard.component';
import { BuyerDashboardComponent } from './pages/buyer-dashboard/buyer-dashboard.component';
import { RegisterLandAssetDialog } from './forms/register-land-asset/register-land-asset.dialog';
import { GetBuyerReqsComponent } from './components/get-buyer-reqs/get-buyer-reqs.component';
import { ViewSellerLandsComponent } from './components/view-seller-lands/view-seller-lands.component';
import { TransferFormComponent } from './forms/transfer-form/transfer-form.component';
import { MyRequestsComponent } from './pages/my-requests/my-requests.component';
import { ViewMapComponent } from './components/view-map/view-map.component';
import { RegistrarDashboardComponent } from './pages/registrar-dashboard/registrar-dashboard.component';
import { RevenueDashboardComponent } from './pages/revenue-dashboard/revenue-dashboard.component';
import { FinanceDashboardComponent } from './pages/finance-dashboard/finance-dashboard.component';
import { DashboardLoginComponent } from './forms/dashboard-login/dashboard-login.component';
import { PendingRegiststrarComponent } from './components/pending-registstrar/pending-registstrar.component';
import { PendingRevenueComponent } from './components/pending-revenue/pending-revenue.component';
import { RequestDetailComponent } from './components/request-detail/request-detail.component';
import { ViewBuyerLandsComponent } from './components/view-buyer-lands/view-buyer-lands.component';
import { TransactionsHistoryComponent } from './components/transactions-history/transactions-history.component';

const routes: Routes = [
  {  
    path: '',
    pathMatch: 'full',
    component: WelcomeComponent 
  },
  {  
    path: 'welcome',
    component: WelcomeComponent 
  },

  {
    path:'dashboard',
    component:DashboardComponent
  },
  { 
    path: 'choices', 
    component: ChoicesComponent 
  }, 
  { 
    path: 'login/:loginType', 
    component: PlatformLoginDialog 
  },
  {
    path:'register/:userType',
    component:PlatformUserRegisisterDialog
  },
  {
    path:'seller-dashboard',
    component:SellerDashboardComponent,
    canActivate: []
  },
  {
    path:'buyer-dashboard',
    component:BuyerDashboardComponent
  },
  {
    path:'register-land-asset',
    component:RegisterLandAssetDialog
  },
  {
    path:'get-buyer-requests',
    component:GetBuyerReqsComponent
  },
  {
    path:'view-seller-lands',
    component:ViewSellerLandsComponent
  },
  {
    path:'transfer-land',
    component:TransferFormComponent
  },
  {
    path:'get-my-requests',
    component:MyRequestsComponent
  },
  {
    path:'view-map',
    component:ViewMapComponent
  },
  { 
    path: 'dashboard-login/:loginType', 
    component: DashboardLoginComponent 
  },
  { 
    path: 'registrar-dashboard', 
    component: RegistrarDashboardComponent 
  },
  { 
    path: 'revenue-dashboard', 
    component: RevenueDashboardComponent 
  },
  {
    path: 'finance-dashboard', 
    component: FinanceDashboardComponent 
  },
  {
    path: 'pending-registrar-req', 
    component: PendingRegiststrarComponent 
  },
  {
    path: 'pending-revenue-req', 
    component: PendingRevenueComponent 
  },
  {
    path: 'request-detail/:landid', 
    component: RequestDetailComponent 
  },
  {
    path:'view-buyer-lands',
    component:ViewBuyerLandsComponent
  },
  {
    path: 'transactions-history',
    component:TransactionsHistoryComponent
  }


  // Add other routes as needed
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
