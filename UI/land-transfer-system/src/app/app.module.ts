import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ChoicesComponent } from './components/choices/choices.component';
import { PlatformLoginDialog } from './forms/platform-login/platform-login.dialog';
import { PlatformComponent } from './pages/platform/platform.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { ReactiveFormsModule } from '@angular/forms';
import { PlatformUserRegisisterDialog } from './forms/platform-user-regisister/platform-user-regisister.dialog';
import { SellerDashboardComponent } from './pages/seller-dashboard/seller-dashboard.component';
import { TransferFormComponent } from './forms/transfer-form/transfer-form.component';
import { BuyerDashboardComponent } from './pages/buyer-dashboard/buyer-dashboard.component';
import { SuccessAlertDialog } from './forms/success-alert/success-alert.dialog';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterLandAssetDialog } from './forms/register-land-asset/register-land-asset.dialog';
import { CommonModule } from '@angular/common';


import {GetBuyerReqsComponent} from './components/get-buyer-reqs/get-buyer-reqs.component';
import { ViewSellerLandsComponent } from './components/view-seller-lands/view-seller-lands.component';
import { FailureAlertComponent } from './forms/failure-alert/failure-alert.component';
import { MyRequestsComponent } from './pages/my-requests/my-requests.component';

import { AgmCoreModule } from '@agm/core';
import { ViewMapComponent } from './components/view-map/view-map.component';
import { RegistrarDashboardComponent } from './pages/registrar-dashboard/registrar-dashboard.component';
import { RevenueDashboardComponent } from './pages/revenue-dashboard/revenue-dashboard.component';
import { DashboardLoginComponent } from './forms/dashboard-login/dashboard-login.component';
import { FinanceDashboardComponent } from './pages/finance-dashboard/finance-dashboard.component';
import { PendingRegiststrarComponent } from './components/pending-registstrar/pending-registstrar.component';
import { PendingRevenueComponent } from './components/pending-revenue/pending-revenue.component';
import { RequestDetailComponent } from './components/request-detail/request-detail.component';
import { ViewBuyerLandsComponent } from './components/view-buyer-lands/view-buyer-lands.component';
import { TransactionsHistoryComponent } from './components/transactions-history/transactions-history.component';



@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    DashboardComponent,
    ChoicesComponent,
    PlatformLoginDialog,
    PlatformComponent,
    PlatformUserRegisisterDialog,
    SellerDashboardComponent,
    TransferFormComponent,
    BuyerDashboardComponent,
    SuccessAlertDialog,
    RegisterLandAssetDialog,
    GetBuyerReqsComponent,
    ViewSellerLandsComponent,
    FailureAlertComponent,
    MyRequestsComponent,
    ViewMapComponent,
    RegistrarDashboardComponent,
    RevenueDashboardComponent,
    DashboardLoginComponent,
    FinanceDashboardComponent,
    PendingRegiststrarComponent,
    PendingRevenueComponent,
    RequestDetailComponent,
    ViewBuyerLandsComponent,
    TransactionsHistoryComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatDialogModule,
    RouterModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCvxM0p2DNC5yYI3-RR2OCBa2kTrVPEyIs',
    }),
    
  ],
  exports:[
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
