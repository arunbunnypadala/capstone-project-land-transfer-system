import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingRegiststrarComponent } from './pending-registstrar.component';

describe('PendingRegiststrarComponent', () => {
  let component: PendingRegiststrarComponent;
  let fixture: ComponentFixture<PendingRegiststrarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendingRegiststrarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PendingRegiststrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
