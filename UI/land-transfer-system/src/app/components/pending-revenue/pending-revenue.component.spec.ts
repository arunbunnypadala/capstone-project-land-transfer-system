import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingRevenueComponent } from './pending-revenue.component';

describe('PendingRevenueComponent', () => {
  let component: PendingRevenueComponent;
  let fixture: ComponentFixture<PendingRevenueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendingRevenueComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PendingRevenueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
