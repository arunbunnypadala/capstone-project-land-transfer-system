import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FailureAlertComponent } from 'src/app/forms/failure-alert/failure-alert.component';
import { SuccessAlertDialog } from 'src/app/forms/success-alert/success-alert.dialog';
import { RegistrarService } from 'src/app/registrar.service';
import { RevenueService } from 'src/app/revenue.service';

@Component({
  selector: 'app-pending-revenue',
  templateUrl: './pending-revenue.component.html',
  styleUrls: ['./pending-revenue.component.scss']
})
export class PendingRevenueComponent {
  allRequests: any[] = [];
  constructor(private router: Router, private dialog: MatDialog, private revService: RevenueService) {
    this.fetchRequests()
  }


  fetchRequests() {
    
    this.revService.getLands().subscribe(
      (data:any) => {
        console.log(data);
        this.allRequests = data;
        this.allRequests = this.allRequests.filter(request =>  (request.registrar_Status.toUpperCase() === 'APPROVED' && request.revenue_Status.toUpperCase() === 'PENDING'));
      },
      (error) =>{

        console.error('Error fetching requests:', error);
      }
    );
  }

  RejectReq(req:any) {
    req.revenue_Status = 'Rejected';
    this.revService.updateRequest(req).subscribe(
      response => {
        console.log('successful', response);
        const dialogRef = this.dialog.open(SuccessAlertDialog)
          setTimeout(() => {
            dialogRef.close();
          }, 2000);
        
        this.router.navigate(['revenue-dashboard'])
      },
      error => {
        console.error('Login failed', error);
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
        }, 2000);
        // Handle login error (e.g., display an error message)
      }
    );
    
  }
  ApproveReq(req:any) {
    req.revenue_Status = 'Approved';
    console.log(req);
    this.revService.updateRequest(req).subscribe(
      response => {
        console.log('successful', response);
        const dialogRef = this.dialog.open(SuccessAlertDialog)
          setTimeout(() => {
            dialogRef.close();
          }, 2000);
        
        this.router.navigate(['revenue-dashboard'])
      },
      error => {
        console.error('Login failed', error);
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
        }, 2000);
        console.error('Login failed', error);
        // Handle login error (e.g., display an error message)
      }
    );
  }
}
