import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { FinanceService } from 'src/app/finance.service';
import { FailureAlertComponent } from 'src/app/forms/failure-alert/failure-alert.component';

@Component({
  selector: 'app-request-detail',
  templateUrl: './request-detail.component.html',
  styleUrls: ['./request-detail.component.scss']
})
export class RequestDetailComponent implements OnInit{
  landid: string | null | undefined = '';
  response:any
  constructor(private route: ActivatedRoute,private finService: FinanceService,private dialog:MatDialog){
    this.route.paramMap.subscribe(params => {
      
      this.landid = params.get('landid');
    });
  }
  ngOnInit() {
    this.fetchRequests();
  }

  fetchRequests() {
    this.finService.getLandById(this.landid).subscribe(
      (data:any) => {
        console.log(data);
        this.response = data;
        //this.allRequests = this.allRequests.filter(request =>  (request.registrar_Status.toUpperCase() === 'APPROVED' && request.revenue_Status.toUpperCase() === 'PENDING'));
      },
      (error) =>{
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
        }, 2000);
        console.error('Error fetching requests:', error);
      }
    );
  }
}
