import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlatformService } from 'src/app/platform.service';

@Component({
  selector: 'app-transactions-history',
  templateUrl: './transactions-history.component.html',
  styleUrls: ['./transactions-history.component.scss']
})
export class TransactionsHistoryComponent implements OnInit{
  transactions: any[]=[];

  constructor(private route: ActivatedRoute,private platService:PlatformService) {
    // Retrieve transactions data from the route state
    this.route.paramMap.subscribe((params) => {
      
       const transactionsParam = params.get('transactions');

  if (transactionsParam !== null) {
   this.transactions = JSON.parse(transactionsParam);
    // Now newArray will only be assigned if transactionsParam is not null
    // Rest of your code handling the newArray goes here
  } else {
    // Handle the case where 'transactions' parameter is null
    console.error("'transactions' parameter is null");
  }
    });
  }
  ngOnInit(): void {
    this.platService.dataArray$.subscribe(dataArray => {
      this.transactions = dataArray;
    });
  }



  
}
