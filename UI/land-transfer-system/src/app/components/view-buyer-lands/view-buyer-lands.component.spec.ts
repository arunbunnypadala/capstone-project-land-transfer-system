import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewBuyerLandsComponent } from './view-buyer-lands.component';

describe('ViewBuyerLandsComponent', () => {
  let component: ViewBuyerLandsComponent;
  let fixture: ComponentFixture<ViewBuyerLandsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewBuyerLandsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewBuyerLandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
