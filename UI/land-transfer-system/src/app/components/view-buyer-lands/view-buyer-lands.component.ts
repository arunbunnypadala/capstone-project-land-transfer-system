import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FailureAlertComponent } from 'src/app/forms/failure-alert/failure-alert.component';
import { PlatformService } from 'src/app/platform.service';

@Component({
  selector: 'app-view-buyer-lands',
  templateUrl: './view-buyer-lands.component.html',
  styleUrls: ['./view-buyer-lands.component.scss']
})
export class ViewBuyerLandsComponent {
  myRequests: any[] = [];
  ownerId : any
  constructor(private platService:PlatformService,private dialog:MatDialog,private router: Router){
    this.ownerId = localStorage["aadhar"]
    this.ownerId = this.ownerId.replace(/"/g, '')
    // this.myRequests = [
    //   { buyerName: 'John Doe', details: 'Request details for John Doe' },
    //   { buyerName: 'Jane Doe', details: 'Request details for Jane Doe' },
    //   // Add more dummy data as needed
    // ];
    
    this.platService.getLands().subscribe(
      (response:any) => {
        console.log('Fecthing lands', response);
        this.myRequests = response
        console.log(this.myRequests,"----------")
        this.myRequests = this.myRequests.filter(requests => this.ownerId == requests.ownerid)
        console.log(this.myRequests,"----------1")
      },
      error => {
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
        }, 2000);
        console.error('Login failed', error);
        // Handle login error (e.g., display an error message)
      }
    );
  }

  viewTransactions(transactions: any[]) {
    this.platService.setDataArray(transactions);
    // Navigate to Transactions History page and pass transactions data
    this.router.navigate(['transactions-history'])
  }
}
