import { Component } from '@angular/core';

@Component({
  selector: 'app-view-map',
  templateUrl: './view-map.component.html',
  styleUrls: ['./view-map.component.scss']
})
export class ViewMapComponent {
  constructor() {}

  ngOnInit(): void {
    // Coordinates of the place to be marked
    const coordinates = { lat: 37.7749, lng: -122.4194 }; // Replace with your desired coordinates

    // Call the function to initialize the map and mark the place
    this.initMap(coordinates);
  }

  initMap(coordinates: { lat: number; lng: number }): void {
    // Create a new map centered at the provided coordinates
    const map = new google.maps.Map(document.getElementById('map') as HTMLElement, {
      center: coordinates,
      zoom: 15,
    });

    // Create a marker at the provided coordinates and set it on the map
    const marker = new google.maps.Marker({
      position: coordinates,
      map: map,
      title: 'Marked Place', // Replace with your desired title
    });
  }
}
