import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FinanceService {

  private apiUrl = 'http://localhost:3005'; 

  constructor(private http: HttpClient) {}

  
  
  updateRequest(req:any){''
    return this.http.post(`${this.apiUrl}/update-request`, req)
  }

  getRequests(){
    return this.http.get(`${this.apiUrl}/get-all-requests`)
  }

  getLandById(id:any){
    return this.http.get(`${this.apiUrl}/get-land-by-id?landid=`+id)
  }

  
}
