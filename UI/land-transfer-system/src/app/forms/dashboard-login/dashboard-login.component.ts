import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { PlatformService } from 'src/app/platform.service';
import { SuccessAlertDialog } from '../success-alert/success-alert.dialog';

@Component({
  selector: 'app-dashboard-login',
  templateUrl: './dashboard-login.component.html',
  styleUrls: ['./dashboard-login.component.scss']
})
export class DashboardLoginComponent {
  loginType: string | null | undefined = '';
  loginForm =  this.fb.group({
    user: ['', Validators.required],
    password: ['', Validators.required],
    type: this.loginType
  })

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.loginType = params.get('loginType');
      this.loginForm.patchValue({ type: this.loginType });
    });
  }


  constructor(private route: ActivatedRoute,private fb: FormBuilder,private router: Router,private platService:PlatformService,private dialog:MatDialog) {
  }

  onSubmit() {
    const dialogRef = this.dialog.open(SuccessAlertDialog)
            setTimeout(() => {
              dialogRef.close();
            }, 2000);
    if(this.loginType=='registrar'){
      this.router.navigate(['registrar-dashboard'])
    }
    if(this.loginType=='revenue'){
      this.router.navigate(['revenue-dashboard'])
    }
    if(this.loginType=='finance'){
      this.router.navigate(['finance-dashboard'])
    }
    
  }
}
