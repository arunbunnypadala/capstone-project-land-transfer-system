import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NavigationStart, Router } from '@angular/router';
import { FailureAlertComponent } from 'src/app/forms/failure-alert/failure-alert.component';
import { SuccessAlertDialog } from 'src/app/forms/success-alert/success-alert.dialog';
import { PlatformService } from 'src/app/platform.service';

interface LandTransaction {
  requestid?: string;
  landid: string;
  buyerid: string;
  ownerid: string;
  requireloan: 'yes' | 'no'; // Assuming requireloan can only be 'yes' or 'no'
}
@Component({
  selector: 'app-buyer-dashboard',
  templateUrl: './buyer-dashboard.component.html',
  styleUrls: ['./buyer-dashboard.component.scss']
})

export class BuyerDashboardComponent {

  allLands: any[] = [];

  constructor(private router: Router, private dialog: MatDialog, private platService: PlatformService) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        console.log('Navigation started');
        console.log('Current route:', event.url);
      }
    });

    // this.buyerRequests = [
    //   { buyerName: 'John Doe', details: 'Request details for John Doe' },
    //   { buyerName: 'Jane Doe', details: 'Request details for Jane Doe' },
    //   // Add more dummy data as needed
    // ];
    this.fetchBuyerRequests()
  }
  logout() {
    localStorage.clear()
    const dialogRef = this.dialog.open(SuccessAlertDialog)
    setTimeout(() => {
      dialogRef.close();
    }, 2000);
    this.router.navigate(['/dashboard'])
  }


  fetchBuyerRequests() {
    this.platService.getLands().subscribe(
      (data: any) => {
        console.log(data);

        if (Array.isArray(data)) {
          data.forEach((land: any, index: number) => {
            console.log(`land ${index}:`, land);
            var loggedInUserId = localStorage['aadhar'];
            loggedInUserId = loggedInUserId.replace(/"/g, '');
            if (land.registrar_Status.toUpperCase() == 'APPROVED' && land.revenue_Status.toUpperCase() == 'APPROVED' && land.ownerid != loggedInUserId){
              this.allLands.push(land)
            }
          });
        } else {
          console.log('The object is not an array.');
        }
      },
      (error) => {
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
          }, 2000); 
        console.error('Error fetching buyer requests:', error);
      }
    );
  }

  RaiseBuyRequest(land: any, loan: 'yes' | 'no') {
    console.log(land)
    var loggedInUserId = localStorage['aadhar'];
    loggedInUserId = loggedInUserId.replace(/"/g, '');
    const request: LandTransaction = {
      landid: land.landid,
      buyerid: loggedInUserId,
      ownerid: land.ownerid,
      requireloan: loan
    };
    land
    this.platService.raiseBuyRequest(request).subscribe(
      response => {
        console.log(response);
        const dialogRef = this.dialog.open(SuccessAlertDialog)
        setTimeout(() => {
          dialogRef.close();
        }, 2000);
      },
      error => {
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
          }, 2000); 
        console.error('Error fetching buyer requests:', error);
      }
    );
  }

  ViewOnMap() {
    this.router.navigate(['/view-map'])
  }


}


