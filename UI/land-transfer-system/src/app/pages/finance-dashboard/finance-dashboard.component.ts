import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FinanceService } from 'src/app/finance.service';
import { FailureAlertComponent } from 'src/app/forms/failure-alert/failure-alert.component';
import { SuccessAlertDialog } from 'src/app/forms/success-alert/success-alert.dialog';
import { RegistrarService } from 'src/app/registrar.service';

@Component({
  selector: 'app-finance-dashboard',
  templateUrl: './finance-dashboard.component.html',
  styleUrls: ['./finance-dashboard.component.scss']
})
export class FinanceDashboardComponent {
  allRequests: any[] = [];
  
  constructor(private router: Router, private dialog: MatDialog, private finService: FinanceService) {
    this.fetchRequests()
  }

  logout() {
    localStorage.clear()
    const dialogRef = this.dialog.open(SuccessAlertDialog)
    setTimeout(() => {
      dialogRef.close();
    }, 2000);
    this.router.navigate(['/dashboard'])
  }


  fetchRequests() {
    this.finService.getRequests().subscribe(
      (data:any) => {
        console.log(data);
        this.allRequests = data;
        this.allRequests = this.allRequests.filter(request =>  request.finance_required.toUpperCase() === 'YES'&& request.bankloan_Status.toUpperCase() === 'PENDING' );
      },
      (error) =>{
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
          }, 2000); 
        console.error('Error fetching requests:', error);
      }
    );
  }

  RejectReq(req:any) {
    req.bankloan_Status = 'Rejected';
    this.finService.updateRequest(req).subscribe(
      response => {
        console.log('successful', response);
        const dialogRef = this.dialog.open(SuccessAlertDialog)
          setTimeout(() => {
            dialogRef.close();
          }, 2000);
        
        this.router.navigate(['finance-dashboard'])
      },
      error => {

        console.error('Login failed', error);
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
        }, 2000);
        // Handle login error (e.g., display an error message)
      }
    );
    
  }
  ApproveReq(req:any) {
    req.bankloan_Status = 'Approved';
    console.log(req);
    this.finService.updateRequest(req).subscribe(
      response => {
        console.log('successful', response);
        const dialogRef = this.dialog.open(SuccessAlertDialog)
          setTimeout(() => {
            dialogRef.close();
          }, 2000);
        
        this.router.navigate(['finance-dashboard'])
      },
      error => {
        console.error('Login failed', error);
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
        }, 2000);
        console.error('Login failed', error);
        // Handle login error (e.g., display an error message)
      }
    );
  }
}
