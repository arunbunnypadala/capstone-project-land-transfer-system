import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NavigationStart, Router } from '@angular/router';
import { FailureAlertComponent } from 'src/app/forms/failure-alert/failure-alert.component';
import { SuccessAlertDialog } from 'src/app/forms/success-alert/success-alert.dialog';
import { PlatformService } from 'src/app/platform.service';

@Component({
  selector: 'app-my-requests',
  templateUrl: './my-requests.component.html',
  styleUrls: ['./my-requests.component.scss']
})

export class MyRequestsComponent {
  allRequests: any[] = [];

  constructor(private router:Router,private dialog:MatDialog,private platService:PlatformService){
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        console.log('Navigation started');
        console.log('Current route:', event.url);
      }
    });
  
    // this.buyerRequests = [
    //   { buyerName: 'John Doe', details: 'Request details for John Doe' },
    //   { buyerName: 'Jane Doe', details: 'Request details for Jane Doe' },
    //   // Add more dummy data as needed
    // ];
    this.fetchBuyerRequests() 
  }
    logout() {
      const dialogRef = this.dialog.open(SuccessAlertDialog)
      setTimeout(() => {
        dialogRef.close();
      }, 2000);
      this.router.navigate(['/login/seller'])
    }
  
  
    fetchBuyerRequests() {
      var loggedInUserId = localStorage['aadhar'];
      loggedInUserId =loggedInUserId.replace(/"/g, '');
      this.platService.getMyReqeusts(loggedInUserId).subscribe(
        (data:any) => {
          console.log(data);
          this.allRequests = data;
        },
        (error) => {
          const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
          }, 2000); 
          console.error('Error fetching buyer requests:', error);
        }
      );
    }
    
  
}


