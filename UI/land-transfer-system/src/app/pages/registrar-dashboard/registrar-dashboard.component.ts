import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FinanceService } from 'src/app/finance.service';
import { FailureAlertComponent } from 'src/app/forms/failure-alert/failure-alert.component';
import { SuccessAlertDialog } from 'src/app/forms/success-alert/success-alert.dialog';
import { RegistrarService } from 'src/app/registrar.service';

@Component({
  selector: 'app-registrar-dashboard',
  templateUrl: './registrar-dashboard.component.html',
  styleUrls: ['./registrar-dashboard.component.scss']
})
export class RegistrarDashboardComponent {
  allRequests: any[] = [];
  constructor(private router: Router, private dialog: MatDialog, private regService: RegistrarService) {
    this.fetchRequests()
  }

  logout() {
    localStorage.clear()
    const dialogRef = this.dialog.open(SuccessAlertDialog)
    setTimeout(() => {
      dialogRef.close();
    }, 2000);
    this.router.navigate(['/dashboard'])
  }


  fetchRequests() {
    
    this.regService.getLands().subscribe(
      (data:any) => {
        console.log(data);
        this.allRequests = data;
        //this.allRequests = this.allRequests.filter(request =>  request.registrar_Status.toUpperCase() === 'PENDING');
      },
      (error) =>{
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
          }, 2000); 
        console.error('Error fetching requests:', error);
      }
    );
  }


  RejectReq(req:any) {
    req.registrar_Status = 'Rejected';
    this.regService.updateRequest(req).subscribe(
      response => {
        console.log('successful', response);
        const dialogRef = this.dialog.open(SuccessAlertDialog)
          setTimeout(() => {
            dialogRef.close();
          }, 2000);
        
        this.router.navigate(['registrar-dashboard'])
      },
      error => {

        console.error('Login failed', error);
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
        }, 2000);
        // Handle login error (e.g., display an error message)
      }
    );
    
  }
  ApproveReq(req:any) {
    req.registrar_Status = 'Approved';
    console.log(req);
    this.regService.updateRequest(req).subscribe(
      response => {
        console.log('successful', response);
        const dialogRef = this.dialog.open(SuccessAlertDialog)
          setTimeout(() => {
            dialogRef.close();
          }, 2000);
        
        this.router.navigate(['registrar-dashboard'])
      },
      error => {
        console.error('Login failed', error);
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
        }, 2000);
        console.error('Login failed', error);
        // Handle login error (e.g., display an error message)
      }
    );
  }


}
