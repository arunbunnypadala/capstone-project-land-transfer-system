import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { FailureAlertComponent } from 'src/app/forms/failure-alert/failure-alert.component';
import { SuccessAlertDialog } from 'src/app/forms/success-alert/success-alert.dialog';
import { RegistrarService } from 'src/app/registrar.service';
import { RevenueService } from 'src/app/revenue.service';

@Component({
  selector: 'app-revenue-dashboard',
  templateUrl: './revenue-dashboard.component.html',
  styleUrls: ['./revenue-dashboard.component.scss']
})
export class RevenueDashboardComponent {
  allRequests: any[] = [];
  
  constructor(private router: Router, private dialog: MatDialog, private revService: RevenueService) {
    this.fetchRequests()
  }

  logout() {
    localStorage.clear()
    const dialogRef = this.dialog.open(SuccessAlertDialog)
    setTimeout(() => {
      dialogRef.close();
    }, 2000);
    this.router.navigate(['/dashboard'])
  }


  fetchRequests() {
    this.revService.getLands().subscribe(
      (data:any) => {
        console.log(data);
        this.allRequests = data;
        //this.allRequests = this.allRequests.filter(request =>  request.registrar_Status.toUpperCase() === 'PENDING');
      },
      (error) =>{
        const dialogRef = this.dialog.open(FailureAlertComponent)
            setTimeout(() => {
              dialogRef.close();
          }, 2000); 
        console.error('Error fetching requests:', error);
      }
    );
  }

  
}
