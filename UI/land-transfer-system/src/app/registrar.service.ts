// login.service.ts

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RegistrarService {
  private apiUrl = 'http://localhost:3000'; 

  constructor(private http: HttpClient) {}

  
  
  updateRequest(req:any){
    return this.http.post(`${this.apiUrl}/approve-land`, req)
  }

  getLands(){
    return this.http.get(`${this.apiUrl}/get-all-lands`)
  }
  


}
