import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RevenueService {

  private apiUrl = 'http://localhost:3001'; 

  constructor(private http: HttpClient) {}

  
  
  updateRequest(req:any){
    return this.http.post(`${this.apiUrl}/approve-land`, req)
  }

  getLands(){
    return this.http.get(`${this.apiUrl}/get-all-lands`)
  }
}
