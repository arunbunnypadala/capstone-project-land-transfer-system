package chaincode

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	"strings"
	"time"
)

var (
	landcollection    = "landcollection"
	usercollection    = "usercollection"
	requestcollection = "requestcollection"
)

// SmartContract provides functions for managing an Asset
type SmartContract struct {
	contractapi.Contract
}

// Asset describes basic details of what makes up a simple asset
// Insert struct field in alphabetic order => to achieve determinism across languages
// golang keeps the order when marshal to json but doesn't order automatically

type LandAsset struct {
	LandId           string        `json:"landid"`
	OwnerId          string        `json:"ownerid"`
	Area             string        `json:"area"`
	Price            string        `json:"price"`
	Location         string        `json:"location"` //coordinates
	Address          string        `json:"address"`  //registered address
	Registrar_Status string        `json:"registrar_Status"`
	Revenue_Status   string        `json:"revenue_Status"`
	Transactions     []Transaction `json:"transactions"`
}

type Transaction struct {
	ID              string    `json:"id"`
	Buyer           string    `json:"buyer"`
	Seller          string    `json:"seller"`
	TransactionDate time.Time `json:"transactionDate"`
	Amount          string    `json:"amount"`
}

type Request struct {
	RequestId       string `json:"requestid"`
	SellerId        string `json:"sellerid"`
	BuyerId         string `json:"buyerid"`
	Finance_Status  string `json:"finance_required"`
	LandId          string `json:"landid"`
	BankLoan_Status string `json:"bankloan_Status"`
}

type User struct {
	Name        string   `json:"name"`
	Type        string   `json:"type"` //buyer or seller
	Email       string   `json:"email"`
	Aadhar      string   `json:"aadhar"`
	Password    string   `json:"password"`
	Address     string   `json:"address"`
	LandIDsList []string `json:"landslist"`
}

// CreateAsset issues a new asset to the world state with given details.
// ...

// CreateLandAsset creates a new LandAsset
func (s *SmartContract) CreatePrivLandAsset(ctx contractapi.TransactionContextInterface, landID string, OwnerId string, area string, price string, location string, address string) error {
	// Check if the land asset with the given ID already exists
	exists, err := s.LandPrivExists(ctx, landID)
	if err != nil {
		return err
	}
	if exists {
		return fmt.Errorf("land asset with ID %s already exists", landID)
	}
	mspID, err := ctx.GetClientIdentity().GetMSPID()
	if err != nil {
		return fmt.Errorf("failed to fectch mspID: %v", err)
	}
	fmt.Println(mspID)

	if mspID == "OperatorMSP" {
		landAsset := LandAsset{
			LandId:           landID,
			OwnerId:          OwnerId,
			Area:             area,
			Price:            price,
			Location:         location,
			Address:          address,
			Registrar_Status: "Pending", // Assuming not approved by default
			Revenue_Status:   "Pending", // Assuming not approved by default
			Transactions:     []Transaction{},
		}

		user, err := s.ReadPrivateUser(ctx, landAsset.OwnerId)

		if err != nil {
			return fmt.Errorf("error fetching user details : %v", err)
		}

		if strings.ToUpper(user.Type) == "SELLER" {
			landAssetJSON, err := json.Marshal(landAsset)
			if err != nil {
				return fmt.Errorf("failed to marshal land asset: %v", err)
			}

			err = ctx.GetStub().PutPrivateData(landcollection, landID, landAssetJSON)
			if err != nil {
				return fmt.Errorf("failed to write to world state: %v", err)
			}
		} else {
			return fmt.Errorf("user is not a seller: %v", err)
		}

	} else {
		return fmt.Errorf("permission denied")
	}

	return nil
}

// ReadLandAsset retrieves a LandAsset from the world state based on ID
func (s *SmartContract) ReadPrivLandAsset(ctx contractapi.TransactionContextInterface, landID string) (*LandAsset, error) {
	landAssetJSON, err := ctx.GetStub().GetPrivateData(landcollection, landID)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if landAssetJSON == nil {
		return nil, fmt.Errorf("land asset with ID %s does not exist", landID)
	}

	var landAsset LandAsset
	err = json.Unmarshal(landAssetJSON, &landAsset)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal land asset: %v", err)
	}

	return &landAsset, nil
}

// DeleteLandAsset deletes a LandAsset from the world state based on ID
func (s *SmartContract) DeletePrivLandAsset(ctx contractapi.TransactionContextInterface, landID string) error {
	// Check if the land asset exists
	_, err := s.ReadPrivLandAsset(ctx, landID)
	if err != nil {
		return err
	}

	// Delete the land asset from the world state
	err = ctx.GetStub().DelPrivateData(landcollection, landID)
	if err != nil {
		return fmt.Errorf("failed to delete land asset: %v", err)
	}

	return nil
}

// ApproveLandAsset marks the approval status of a LandAsset as true

func (s *SmartContract) ApprovePrivLandAsset(ctx contractapi.TransactionContextInterface, landID string, status string) error {
	landAsset, err := s.ReadPrivLandAsset(ctx, landID)
	if err != nil {
		return err
	}

	mspId, err := ctx.GetClientIdentity().GetMSPID()
	fmt.Println(mspId)
	if err != nil {
		return fmt.Errorf("error getting mspID: %v", err)
	}

	if mspId == "RegulatorMSP" {
		if strings.ToUpper(status) == "APPROVED" {
			// Update the approval status to true
			user, err := s.ReadPrivateUser(ctx, landAsset.OwnerId)
			if err != nil {
				return fmt.Errorf("error reading owner details %v", err)
			}
			if strings.ToUpper(user.Type) == "SELLER" {
				user.LandIDsList = addToSlice(user.LandIDsList, landAsset.LandId)
				err := s.UpdatePrivateUser(ctx, user.Aadhar, "", "", "", "", "", user.LandIDsList)
				if err != nil {
					return fmt.Errorf("error in updating seller with approved LandIds %s", err)
				}
				landAsset.Registrar_Status = status
			} else {
				return fmt.Errorf("cannot register land as user is not seller")
			}

		} else if strings.ToUpper(status) == "REJECTED" {
			landAsset.Registrar_Status = status
		} else {
			return fmt.Errorf("invalid status")
		}

		// Marshal the updated land asset and write to the world state
		updatedLandAssetJSON, err := json.Marshal(landAsset)
		if err != nil {
			return fmt.Errorf("failed to marshal updated land asset: %v", err)
		}

		err = ctx.GetStub().PutPrivateData(landcollection, landID, updatedLandAssetJSON)
		if err != nil {
			return fmt.Errorf("failed to write to world state: %v", err)
		}
	} else if mspId == "RevenueMSP" {
		if strings.ToUpper(status) == "APPROVED" {
			// Update the approval status to true
			user, err := s.ReadPrivateUser(ctx, landAsset.OwnerId)
			if err != nil {
				return fmt.Errorf("error reading owner details %v", err)
			}

			if strings.ToUpper(user.Type) == "SELLER" {
				landassetFromLG, err := s.ReadPrivLandAsset(ctx, landAsset.LandId)
				if err != nil {
					return fmt.Errorf("error in reading land%s", err)
				}
				if strings.ToUpper(landassetFromLG.Registrar_Status) == "APPROVED" {
					landAsset.Revenue_Status = status
				} else {
					return fmt.Errorf("error, registrar approval is required")
				}
			} else {
				return fmt.Errorf("cannot register land as user is not seller")
			}
		} else if strings.ToUpper(status) == "REJECTED" {
			landAsset.Revenue_Status = status
		} else {
			return fmt.Errorf("invalid status")
		}

		// Marshal the updated land asset and write to the world state
		updatedLandAssetJSON, err := json.Marshal(landAsset)
		if err != nil {
			return fmt.Errorf("failed to marshal updated land asset: %v", err)
		}

		err = ctx.GetStub().PutPrivateData(landcollection, landID, updatedLandAssetJSON)
		if err != nil {
			return fmt.Errorf("failed to write to world state: %v", err)
		}
	} else {
		return fmt.Errorf("permission denied")
	}
	return nil
}

func (s *SmartContract) UpdatePrivLandAsset(ctx contractapi.TransactionContextInterface, landID string, newArea string, newPrice string, newOwnerId string, newLocation string, newAddress string, transaction *Transaction) error {
	landAsset, err := s.ReadPrivLandAsset(ctx, landID)
	if err != nil {
		return err
	}

	// Update fields if new values are provided
	if newArea != "" {
		landAsset.Area = newArea
	}
	if newPrice != "" {
		landAsset.Price = newPrice
	}
	if newOwnerId != "" {
		landAsset.OwnerId = newOwnerId
	}
	if newLocation != "" {
		landAsset.Location = newLocation
	}
	if newAddress != "" {
		landAsset.Address = newAddress
	}
	if transaction != nil {
		landAsset.Transactions = append(landAsset.Transactions, *transaction)
	}

	// Marshal the updated land asset and write to the world state
	updatedLandAssetJSON, err := json.Marshal(landAsset)
	if err != nil {
		return fmt.Errorf("failed to marshal updated land asset: %v", err)
	}

	err = ctx.GetStub().PutPrivateData(landcollection, landID, updatedLandAssetJSON)
	if err != nil {
		return fmt.Errorf("failed to write to Land PDC: %v", err)
	}

	return nil
}

// AssetExists returns true when asset with given ID exists in world state

func (s *SmartContract) LandPrivExists(ctx contractapi.TransactionContextInterface, id string) (bool, error) {
	assetJSON, err := ctx.GetStub().GetPrivateData(landcollection, id)
	if err != nil {
		return false, fmt.Errorf("failed to read from world state: %v", err)
	}

	return assetJSON != nil, nil
}

// GetAllAssets returns all assets found in world state

func (s *SmartContract) GetAllPrivateLandAssets(ctx contractapi.TransactionContextInterface) ([]*LandAsset, error) {
	// range query with empty string for startKey and endKey does an
	// open-ended query of all assets in the chaincode namespace.
	resultsIterator, err := ctx.GetStub().GetPrivateDataByRange(landcollection, "", "")
	if err != nil {
		return nil, err
	}
	defer resultsIterator.Close()

	var assets []*LandAsset
	for resultsIterator.HasNext() {
		queryResponse, err := resultsIterator.Next()
		if err != nil {
			return nil, err
		}

		var asset LandAsset
		err = json.Unmarshal(queryResponse.Value, &asset)
		if err != nil {
			return nil, err
		}
		fmt.Println(asset)
		assets = append(assets, &asset)
	}

	return assets, nil
}

func (s *SmartContract) CreatePrivateUser(ctx contractapi.TransactionContextInterface, name string, userType string, email string, aadhar string, password string, address string) error {
	// Check if the user with the given email already exists
	exists, err := s.UserPrivateExists(ctx, aadhar)
	if err != nil {
		return err
	}
	if exists {
		return fmt.Errorf("user with aadhar %s already exists", aadhar)
	}
	mspId, err := ctx.GetClientIdentity().GetMSPID()
	if err != nil {
		return fmt.Errorf("error fetching mspID")
	}
	if mspId == "OperatorMSP" {
		user := User{
			Name:        name,
			Type:        userType,
			Email:       email,
			Aadhar:      aadhar,
			Password:    password,
			Address:     address,
			LandIDsList: []string{}, // Assuming an empty list by default
		}

		userJSON, err := json.Marshal(user)
		if err != nil {
			return fmt.Errorf("failed to marshal user: %v", err)
		}

		err = ctx.GetStub().PutPrivateData(usercollection, aadhar, userJSON)
		if err != nil {
			return fmt.Errorf("failed to write to User PDC: %v", err)
		}
	} else {
		return fmt.Errorf("permisssion denied")
	}

	return nil
}

// ReadUser retrieves a User from the world state based on email

func (s *SmartContract) ReadPrivateUser(ctx contractapi.TransactionContextInterface, aadhar string) (*User, error) {
	userJSON, err := ctx.GetStub().GetPrivateData(usercollection, aadhar)
	if err != nil {
		return nil, fmt.Errorf("failed to read from user PDC: %v", err)
	}
	if userJSON == nil {
		return nil, fmt.Errorf("user with email %s does not exist", aadhar)
	}

	var user User
	err = json.Unmarshal(userJSON, &user)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal user: %v", err)
	}

	return &user, nil
}

// UpdateUser updates the details of an existing User

func (s *SmartContract) UpdatePrivateUser(ctx contractapi.TransactionContextInterface, aadhar string, newName string, newType string, newAadhar string, newPassword string, newAddress string, newLandIds []string) error {
	user, err := s.ReadPrivateUser(ctx, aadhar)
	if err != nil {
		return err
	}
	fmt.Sprintf("Original User ::::::::::::: %v", user)

	// Update fields if new values are provided
	if newName != "" {
		user.Name = newName
	}
	if newType != "" {
		user.Type = newType
	}
	if newAadhar != "" {
		user.Aadhar = newAadhar
	}
	if newPassword != "" {
		user.Password = newPassword
	}
	if newAddress != "" {
		user.Address = newAddress
	}
	if newLandIds != nil {
		user.LandIDsList = newLandIds
	}
	fmt.Sprintf("Updated User ::::::::::::: %v", user)
	// Marshal the updated user and write to the world state
	updatedUserJSON, err := json.Marshal(user)
	if err != nil {
		return fmt.Errorf("failed to marshal updated user: %v", err)
	}

	err = ctx.GetStub().PutPrivateData(usercollection, aadhar, updatedUserJSON)
	if err != nil {
		return fmt.Errorf("failed to write to user PDC: %v", err)
	}

	return nil
}

// DeleteUser deletes a User from the world state based on email

func (s *SmartContract) DeletePrivateUser(ctx contractapi.TransactionContextInterface, email string) error {
	// Check if the user exists
	_, err := s.ReadPrivateUser(ctx, email)
	if err != nil {
		return err
	}

	// Delete the user from the user PDC
	err = ctx.GetStub().DelPrivateData(usercollection, email)
	if err != nil {
		return fmt.Errorf("failed to delete user: %v", err)
	}

	return nil
}

// UserExists checks if a User with the given email already exists

func (s *SmartContract) UserPrivateExists(ctx contractapi.TransactionContextInterface, aadhar string) (bool, error) {
	userJSON, err := ctx.GetStub().GetPrivateData(usercollection, aadhar)
	if err != nil {
		return false, fmt.Errorf("failed to read from user PDC: %v", err)
	}
	return userJSON != nil, nil
}

// GetAllUsers returns all user records from the ledger

func (s *SmartContract) GetAllPrivateUsers(ctx contractapi.TransactionContextInterface) ([]*User, error) {
	resultsIterator, err := ctx.GetStub().GetPrivateDataByRange(usercollection, "", "")
	if err != nil {
		return nil, fmt.Errorf("failed to get state by range: %v", err)
	}
	defer resultsIterator.Close()

	var users []*User

	for resultsIterator.HasNext() {
		result, err := resultsIterator.Next()
		if err != nil {
			return nil, fmt.Errorf("failed to get next result: %v", err)
		}

		var user User
		err = json.Unmarshal(result.Value, &user)
		if err != nil {
			return nil, fmt.Errorf("failed to unmarshal user: %v", err)
		}

		users = append(users, &user)
	}

	return users, nil
}

func (s *SmartContract) CreatePrivateRequest(ctx contractapi.TransactionContextInterface, requestID string, sellerid string, buyerid string, financeRequired string, landid string) error {
	// Check if the invoking organization has the required MSP ID
	//if err := s.checkMSPID(ctx, "RequiredMSPID"); err != nil {
	//	return err
	//}

	// Check if the request with the given ID already exists
	mspID, err := ctx.GetClientIdentity().GetMSPID()
	if err != nil {
		return fmt.Errorf("failed to get current MSPID %v", err)
	}
	if mspID == "OperatorMSP" {
		exists, err := s.PrivateRequestExists(ctx, requestID)
		if err != nil {
			return err
		}
		if exists {
			return fmt.Errorf("request with ID %s already exists", requestID)
		}
		//validate buyer
		_, err = s.ReadPrivateUser(ctx, buyerid)
		if err != nil {
			return fmt.Errorf("error Buyer user doesn't exist: %v", err)
		}

		user, err := s.ReadPrivateUser(ctx, sellerid)
		if err != nil {
			return fmt.Errorf("error Seller user doesn't exist: %v", err)
		}

		ownsLand := false
		for _, land := range user.LandIDsList {
			if land == landid {
				ownsLand = true
			}
		}
		if ownsLand {
			request := Request{
				RequestId:       requestID,
				SellerId:        sellerid,
				BuyerId:         buyerid,
				Finance_Status:  financeRequired,
				LandId:          landid,
				BankLoan_Status: "pending", // Assuming not approved by default
			}

			requestJSON, err := json.Marshal(request)
			if err != nil {
				return fmt.Errorf("failed to marshal request: %v", err)
			}

			err = ctx.GetStub().PutPrivateData(requestcollection, requestID, requestJSON)
			if err != nil {
				return fmt.Errorf("failed to write to requests PDC: %v", err)
			} else {
				landFromLG, err := s.ReadPrivLandAsset(ctx, landid)
				if err != nil {
					return fmt.Errorf("failed to fetch land: %v", err)
				}

				if strings.ToUpper(request.Finance_Status) == "NO" {
					if strings.ToUpper(landFromLG.Registrar_Status) == "APPROVED" {
						if strings.ToUpper(landFromLG.Revenue_Status) == "APPROVED" {
							err := s.TransferLandOwnership(ctx, request)
							if err != nil {
								return fmt.Errorf("error in transfering land asset: %v", err)
							}
						} else {
							return fmt.Errorf("error, revenue dept approval is required")
						}
					} else {
						return fmt.Errorf("error, land is not registered by registrar yet")
					}
				}
			}

		} else {
			return fmt.Errorf("error, seller doesn't own this land")
		}

		return nil
	} else {
		return fmt.Errorf("Permisssion Denied ")
	}

}

// ReadRequest retrieves a Request from the world state based on request ID

func (s *SmartContract) ReadPrivateRequest(ctx contractapi.TransactionContextInterface, requestID string) (*Request, error) {
	requestJSON, err := ctx.GetStub().GetPrivateData(requestcollection, requestID)
	if err != nil {
		return nil, fmt.Errorf("failed to read from requests PDC: %v", err)
	}
	if requestJSON == nil {
		return nil, fmt.Errorf("request with ID %s does not exist", requestID)
	}

	var request Request
	err = json.Unmarshal(requestJSON, &request)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshal request: %v", err)
	}

	return &request, nil
}

// UpdateRequest updates the details of an existing Request

func (s *SmartContract) UpdatePrivateRequest(ctx contractapi.TransactionContextInterface, requestID string, status string) error {
	request, err := s.ReadPrivateRequest(ctx, requestID)
	if err != nil {
		return err
	}

	mspID, err := ctx.GetClientIdentity().GetMSPID()

	if mspID == "FinanceMSP" {
		if strings.ToUpper(request.Finance_Status) == "YES" {
			if strings.ToUpper(status) == "APPROVED" {
				landFromLG, err := s.ReadPrivLandAsset(ctx, request.LandId)
				if err != nil {
					return fmt.Errorf("failed to fetch land: %v", err)
				}
				if strings.ToUpper(landFromLG.Registrar_Status) == "APPROVED" {
					if strings.ToUpper(landFromLG.Revenue_Status) == "APPROVED" {
						request.BankLoan_Status = status
						err := s.TransferLandOwnership(ctx, *request)
						if err != nil {
							return fmt.Errorf("error in transfering land asset: %v", err)
						}
					} else {
						return fmt.Errorf("error, revenue dept approval is required")
					}
				} else {
					return fmt.Errorf("error, land is not registered by registrar yet")
				}

			} else if strings.ToUpper(status) == "REJECTED" {
				request.BankLoan_Status = status
			} else {
				return fmt.Errorf("Invalid Status ")
			}
		} else {
			return fmt.Errorf("error Finance_Status is 'NO'")
		}
	} else {
		return fmt.Errorf("Permission Denied ")
	}

	// Marshal the updated request and write to the world state
	updatedRequestJSON, err := json.Marshal(request)
	if err != nil {
		return fmt.Errorf("failed to marshal updated request: %v", err)
	}

	err = ctx.GetStub().PutPrivateData(requestcollection, requestID, updatedRequestJSON)
	if err != nil {
		return fmt.Errorf("failed to write to requests PDC: %v", err)
	}

	return nil
}

// DeleteRequest deletes a Request from the world state based on request ID

func (s *SmartContract) DeletePrivateRequest(ctx contractapi.TransactionContextInterface, requestID string) error {
	// Check if the request exists
	_, err := s.ReadPrivateRequest(ctx, requestID)
	if err != nil {
		return err
	}

	// Delete the request from the world state
	err = ctx.GetStub().DelPrivateData(requestcollection, requestID)
	if err != nil {
		return fmt.Errorf("failed to delete request: %v", err)
	}

	return nil
}

// RequestExists checks if a Request with the given ID already exists

func (s *SmartContract) PrivateRequestExists(ctx contractapi.TransactionContextInterface, requestID string) (bool, error) {
	requestJSON, err := ctx.GetStub().GetPrivateData(requestcollection, requestID)
	if err != nil {
		return false, fmt.Errorf("failed to read from requests PDC: %v", err)
	}

	return requestJSON != nil, nil
}

// GetAllRequests returns all request records from the ledger

func (s *SmartContract) GetAllPrivateRequests(ctx contractapi.TransactionContextInterface) ([]*Request, error) {
	resultsIterator, err := ctx.GetStub().GetPrivateDataByRange(requestcollection, "", "")
	if err != nil {
		return nil, fmt.Errorf("failed to get state by range: %v", err)
	}
	defer resultsIterator.Close()

	var requests []*Request

	for resultsIterator.HasNext() {
		result, err := resultsIterator.Next()
		if err != nil {
			return nil, fmt.Errorf("failed to get next result: %v", err)
		}

		var request Request
		err = json.Unmarshal(result.Value, &request)
		if err != nil {
			return nil, fmt.Errorf("failed to unmarshal request: %v", err)
		}

		requests = append(requests, &request)
	}

	return requests, nil
}

func (s *SmartContract) GetAllPrivateRequestsByUserId(ctx contractapi.TransactionContextInterface, userid string) ([]*Request, error) {
	resultsIterator, err := ctx.GetStub().GetPrivateDataByRange(requestcollection, "", "")
	if err != nil {
		return nil, fmt.Errorf("failed to get state by range: %v", err)
	}
	defer resultsIterator.Close()

	var requests []*Request

	for resultsIterator.HasNext() {
		result, err := resultsIterator.Next()
		if err != nil {
			return nil, fmt.Errorf("failed to get next result: %v", err)
		}

		var request Request
		err = json.Unmarshal(result.Value, &request)
		if err != nil {
			return nil, fmt.Errorf("failed to unmarshal request: %v", err)
		}
		if strings.ToUpper(request.BuyerId) == strings.ToUpper(userid) || strings.ToUpper(request.SellerId) == strings.ToUpper(userid) {
			requests = append(requests, &request)
		}
	}

	return requests, nil
}

func (s *SmartContract) checkMSPID(ctx contractapi.TransactionContextInterface, requiredMSPID string) error {
	mspID, err := ctx.GetClientIdentity().GetMSPID()
	if err != nil {
		return fmt.Errorf("failed to get MSP ID: %v", err)
	}

	if mspID != requiredMSPID {
		return fmt.Errorf("only orgs with MSP ID %s are allowed to execute this function", requiredMSPID)
	}

	return nil
}

func (s *SmartContract) TransferLandOwnership(ctx contractapi.TransactionContextInterface, request Request) error {

	mspId, err := ctx.GetClientIdentity().GetMSPID()

	if err != nil {
		return fmt.Errorf("error fetching MspId")
	}

	if mspId == "OperatorMSP" || mspId == "FinanceMSP" {
		asset, err := s.ReadPrivLandAsset(ctx, request.LandId)
		if err != nil {
			return fmt.Errorf("error fetching land asset : %v", err)
		}
		transaction := Transaction{
			ID:              uuid.New().String(),
			Buyer:           request.BuyerId,
			Seller:          request.SellerId,
			TransactionDate: time.Now(),
			Amount:          asset.Price,
		}
		err = s.UpdatePrivLandAsset(ctx, asset.LandId, "", "", request.BuyerId, "", "", &transaction)
		if err != nil {
			return fmt.Errorf("error Updating Land Asset with new owner : %v", err)
		}
		//update seller owned land list
		seller, err := s.ReadPrivateUser(ctx, request.SellerId)
		if err != nil {
			return fmt.Errorf("error while fetching seller with id: %s :: %v", request.SellerId, err)
		}
		seller.LandIDsList, err = RemoveLandFromLandList(seller.LandIDsList, request.LandId)
		if err != nil {
			return fmt.Errorf("%s", err.Error())
		}
		err = s.UpdatePrivateUser(ctx, seller.Aadhar, "", "", "", "", "", seller.LandIDsList)
		if err != nil {
			return err
		}
		//update buyer owned land list
		buyer, err := s.ReadPrivateUser(ctx, request.BuyerId)
		if err != nil {
			return fmt.Errorf("error while fetching buyer with id: %s :: %v", request.BuyerId, err)
		}
		buyer.LandIDsList = addToSlice(buyer.LandIDsList, request.LandId)
		err = s.UpdatePrivateUser(ctx, buyer.Aadhar, "", "", "", "", "", buyer.LandIDsList)
		if err != nil {
			return err
		}

	}
	return nil
}

func addToSlice(slice []string, element string) []string {
	// Check if the element already exists in the slice
	for _, v := range slice {
		if v == element {
			return slice // Element already exists, return the slice unchanged
		}
	}
	// Append the element to the slice since it doesn't already exist
	return append(slice, element)
}

func RemoveLandFromLandList(slice []string, valueToRemove string) ([]string, error) {
	var indexToRemove int
	found := false

	// Find the index of the element to remove
	for i, element := range slice {
		if element == valueToRemove {
			indexToRemove = i
			found = true
			break
		}
	}

	// If the element is found, remove it
	if found {
		slice = append(slice[:indexToRemove], slice[indexToRemove+1:]...)
	} else {
		fmt.Println("Element not found in slice")
		return slice, errors.New("error occurred, Land not found in LandList of Seller")
	}

	return slice, nil
}
