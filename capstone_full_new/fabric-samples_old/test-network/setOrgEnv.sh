#!/bin/bash
#
# SPDX-License-Identifier: Apache-2.0




# default to using Org1
ORG=${1:-Org1}

# Exit on first error, print all commands.
set -e
set -o pipefail

# Where am I?
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"

ORDERER_CA=${DIR}/test-network/organizations/ordererOrganizations/landrecords.com/tlsca/tlsca.landrecords.com-cert.pem
PEER0_ORG1_CA=${DIR}/test-network/organizations/peerOrganizations/operator.landrecords.com/tlsca/tlsca.operator.landrecords.com-cert.pem
PEER0_ORG2_CA=${DIR}/test-network/organizations/peerOrganizations/regulator.landrecords.com/tlsca/tlsca.regulator.landrecords.com-cert.pem
PEER0_ORG3_CA=${DIR}/test-network/organizations/peerOrganizations/revenue.landrecords.com/tlsca/tlsca.revenue.landrecords.com-cert.pem
PEER0_ORG4_CA=${DIR}/test-network/organizations/peerOrganizations/finance.landrecords.com/tlsca/tlsca.finance.landrecords.com-cert.pem


if [[ ${ORG,,} == "operator" || ${ORG,,} == "digibank" ]]; then

   CORE_PEER_LOCALMSPID=OperatorMSP
   CORE_PEER_MSPCONFIGPATH=${DIR}/test-network/organizations/peerOrganizations/operator.landrecords.com/users/Admin@operator.landrecords.com/msp
   CORE_PEER_ADDRESS=localhost:7051
   CORE_PEER_TLS_ROOTCERT_FILE=${DIR}/test-network/organizations/peerOrganizations/operator.landrecords.com/tlsca/tlsca.operator.landrecords.com-cert.pem

elif [[ ${ORG,,} == "regulator" || ${ORG,,} == "magnetocorp" ]]; then

   CORE_PEER_LOCALMSPID=RegulatorMSP
   CORE_PEER_MSPCONFIGPATH=${DIR}/test-network/organizations/peerOrganizations/regulator.landrecords.com/users/Admin@regulator.landrecords.com/msp
   CORE_PEER_ADDRESS=localhost:9051
   CORE_PEER_TLS_ROOTCERT_FILE=${DIR}/test-network/organizations/peerOrganizations/regulator.landrecords.com/tlsca/tlsca.regulator.landrecords.com-cert.pem

elif [[ ${ORG,,} == "revenue" || ${ORG,,} == "magnetocorp" ]]; then

   CORE_PEER_LOCALMSPID=RevenueMSP
   CORE_PEER_MSPCONFIGPATH=${DIR}/test-network/organizations/peerOrganizations/revenue.landrecords.com/users/Admin@revenue.landrecords.com/msp
   CORE_PEER_ADDRESS=localhost:11051
   CORE_PEER_TLS_ROOTCERT_FILE=${DIR}/test-network/organizations/peerOrganizations/revenue.landrecords.com/tlsca/tlsca.revenue.landrecords.com-cert.pem

elif [[ ${ORG,,} == "finance" || ${ORG,,} == "magnetocorp" ]]; then

   CORE_PEER_LOCALMSPID=FinanceMSP
   CORE_PEER_MSPCONFIGPATH=${DIR}/test-network/organizations/peerOrganizations/finance.landrecords.com/users/Admin@finance.landrecords.com/msp
   CORE_PEER_ADDRESS=localhost:13051
   CORE_PEER_TLS_ROOTCERT_FILE=${DIR}/test-network/organizations/peerOrganizations/finance.landrecords.com/tlsca/tlsca.finance.landrecords.com-cert.pem

else
   echo "Unknown \"$ORG\", please choose Org1/Digibank or Org2/Magnetocorp"
   echo "For landrecords to get the environment variables to set upa Org2 shell environment run:  ./setOrgEnv.sh Org2"
   echo
   echo "This can be automated to set them as well with:"
   echo
   echo 'export $(./setOrgEnv.sh Org2 | xargs)'
   exit 1
fi

# output the variables that need to be set
echo "CORE_PEER_TLS_ENABLED=true"
echo "ORDERER_CA=${ORDERER_CA}"
echo "PEER0_ORG1_CA=${PEER0_ORG1_CA}"
echo "PEER0_ORG2_CA=${PEER0_ORG2_CA}"
echo "PEER0_ORG3_CA=${PEER0_ORG3_CA}"
echo "PEER0_ORG4_CA=${PEER0_ORG4_CA}"

echo "CORE_PEER_MSPCONFIGPATH=${CORE_PEER_MSPCONFIGPATH}"
echo "CORE_PEER_ADDRESS=${CORE_PEER_ADDRESS}"
echo "CORE_PEER_TLS_ROOTCERT_FILE=${CORE_PEER_TLS_ROOTCERT_FILE}"

echo "CORE_PEER_LOCALMSPID=${CORE_PEER_LOCALMSPID}"
