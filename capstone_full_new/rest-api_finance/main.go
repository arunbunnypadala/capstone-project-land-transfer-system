package main

import (
	"fmt"
	"rest-api-go/web"
)

func main() {
	//Initialize setup for Finance
	cryptoPath := "/home/arun.padala@npci.org.in/Desktop/capstone/capstone_full_new/fabric-samples_old/test-network/organizations/peerOrganizations/finance.landrecords.com"
	orgConfig := web.OrgSetup{
		OrgName:      "finance",
		MSPID:        "FinanceMSP",
		CertPath:     cryptoPath + "/users/User1@finance.landrecords.com/msp/signcerts/User1@finance.landrecords.com-cert.pem",
		KeyPath:      cryptoPath + "/users/User1@finance.landrecords.com/msp/keystore/",
		TLSCertPath:  cryptoPath + "/peers/peer0.finance.landrecords.com/tls/ca.crt",
		PeerEndpoint: "localhost:13051",
		GatewayPeer:  "peer0.finance.landrecords.com",
	}

	orgSetup, err := web.Initialize(orgConfig)
	if err != nil {
		fmt.Println("Error initializing setup for Finance: ", err)
	}
	web.Serve(web.OrgSetup(*orgSetup))
}
