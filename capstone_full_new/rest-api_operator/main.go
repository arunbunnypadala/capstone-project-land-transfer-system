package main

import (
	"fmt"
	"rest-api-go/web"
)

func main() {
	//Initialize setup for Operator
	cryptoPath := "/home/arun.padala@npci.org.in/Desktop/capstone/capstone_full_new/fabric-samples_old/test-network/organizations/peerOrganizations/operator.landrecords.com"
	orgConfig := web.OrgSetup{
		OrgName:      "operator",
		MSPID:        "OperatorMSP",
		CertPath:     cryptoPath + "/users/User1@operator.landrecords.com/msp/signcerts/User1@operator.landrecords.com-cert.pem",
		KeyPath:      cryptoPath + "/users/User1@operator.landrecords.com/msp/keystore/",
		TLSCertPath:  cryptoPath + "/peers/peer0.operator.landrecords.com/tls/ca.crt",
		PeerEndpoint: "localhost:7051",
		GatewayPeer:  "peer0.operator.landrecords.com",
	}

	orgSetup, err := web.Initialize(orgConfig)
	if err != nil {
		fmt.Println("Error initializing setup for Operator: ", err)
	}
	web.Serve(web.OrgSetup(*orgSetup))
}
