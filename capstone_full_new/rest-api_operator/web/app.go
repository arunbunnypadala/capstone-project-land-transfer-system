package web

import (
	"fmt"
	"net/http"

	"github.com/hyperledger/fabric-gateway/pkg/client"
)

// OrgSetup contains organization's config to interact with the network.
type OrgSetup struct {
	OrgName      string
	MSPID        string
	CryptoPath   string
	CertPath     string
	KeyPath      string
	TLSCertPath  string
	PeerEndpoint string
	GatewayPeer  string
	Gateway      client.Gateway
}

// Serve starts http web server.
func Serve(setups OrgSetup) {
	// Create a new router
	mux := http.NewServeMux()

	// Attach CORS middleware
	handler := corsMiddleware(mux)

	mux.HandleFunc("/buy-land", setups.BuyLand)
	mux.HandleFunc("/get-my-request", setups.GetBuyerRequest)

	mux.HandleFunc("/register-user", setups.RegisterUser)
	mux.HandleFunc("/get-user", setups.FetchUser)
	mux.HandleFunc("/login", setups.Login)

	mux.HandleFunc("/approve-land-request", setups.UpdateBuyerRequest)
	mux.HandleFunc("/raise-land-request", setups.SellerRaiseRequest)
	mux.HandleFunc("/get-land-request", setups.FetchRequestsByUserId)
	mux.HandleFunc("/get-request-by-id", setups.FetchRequestById)

	mux.HandleFunc("/register-land", setups.RegisterLand)
	mux.HandleFunc("/get-land-by-id", setups.FetchLandById)
	mux.HandleFunc("/get-all-lands", setups.FetchAllLands)

	fmt.Println("Listening (http://localhost:3006/)...")
	if err := http.ListenAndServe(":3006", handler); err != nil {
		fmt.Println(err)
	}
}

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")

		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		next.ServeHTTP(w, r)
	})
}
