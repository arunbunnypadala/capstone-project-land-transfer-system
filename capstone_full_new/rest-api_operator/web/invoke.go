package web

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/hyperledger/fabric-gateway/pkg/client"
	"io"
	"net/http"
	"rest-api-go/constants"
	"strings"
)

type requestBody struct {
	RequestId   string `json:"requestId"`
	LandId      string `json:"landid"`
	BuyerId     string `json:"buyerid"`
	OwnerId     string `json:"ownerid"`
	RequireLoan string `json:"requireloan"`
	Status      string `json:"status"`
}

type User struct {
	Name     string `json:"name"`
	Type     string `json:"type"` //buyer or seller
	Email    string `json:"email"`
	Aadhar   string `json:"aadhar"`
	Password string `json:"password"`
	Address  string `json:"address"`
}

type Land struct {
	LandId   string `json:"landid"`
	OwnerId  string `json:"ownerid"`
	Area     string `json:"area"`
	Price    string `json:"price"`
	Location string `json:"location"` //coordinates
	Address  string `json:"address"`
}
type Response struct {
	Status string `json:"Status"`
}

var (
	buyerRequests = make([]requestBody, 0)
)

func (setup *OrgSetup) Login(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received Login request")
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	var user User
	err = json.Unmarshal(body, &user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "ReadPrivateUser"
	args := user.Aadhar
	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %s\n", channelID, chainCodeName, function, args)
	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)
	evaluateResponse, err := contract.EvaluateTransaction(function, args)
	var userFromLG User
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	err = json.Unmarshal(evaluateResponse, &userFromLG)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	if user.Password == userFromLG.Password {
		if strings.ToUpper(user.Type) == strings.ToUpper(userFromLG.Type) {
			w.WriteHeader(http.StatusOK)
			resp := Response{Status: constants.SUCCESS}
			bytes, _ := json.Marshal(resp)
			w.Write(bytes)
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			resp := Response{Status: "User Type Mismatch"}
			bytes, _ := json.Marshal(resp)
			w.Write(bytes)
		}
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: "Login Failed"}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
	}
	return
}

func (setup *OrgSetup) BuyLand(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received Buy Land request")
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	var request requestBody
	err = json.Unmarshal(body, &request)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	request.Status = "pending"
	request.RequestId = uuid.New().String()
	buyerRequests = append(buyerRequests, request)
	w.WriteHeader(http.StatusOK)
	resp := Response{Status: constants.SUCCESS}
	bytes, _ := json.Marshal(resp)
	w.Write(bytes)
	return

}
func (setup *OrgSetup) GetBuyerRequest(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received Get Buy Land request")
	queryParams := r.URL.Query()
	fmt.Println(queryParams)
	paramValue := queryParams.Get("ownerid")
	paramValue2 := queryParams.Get("buyerid")
	fmt.Println("paramValue", paramValue)
	result := make([]requestBody, 0)
	for _, request := range buyerRequests {
		if request.OwnerId == paramValue {
			result = append(result, request)
		} else if request.BuyerId == paramValue2 {
			chainCodeName := constants.ChaincodeName
			channelID := "mychannel"
			function := "ReadPrivLandAsset"
			args := request.LandId
			fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %s\n", channelID, chainCodeName, function, args)
			network := setup.Gateway.GetNetwork(channelID)
			contract := network.GetContract(chainCodeName)
			evaluateResponse, err := contract.EvaluateTransaction(function, args)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				resp := Response{Status: err.Error()}
				bytes, _ := json.Marshal(resp)
				w.Write(bytes)
				return
			}
			var land Land
			err = json.Unmarshal(evaluateResponse, &land)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				resp := Response{Status: err.Error()}
				bytes, _ := json.Marshal(resp)
				w.Write(bytes)
				return
			}
			if land.OwnerId == paramValue2 {
				request.Status = "Approved"
			}
			result = append(result, request)
		}
	}
	bytes, err := json.Marshal(result)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
	return
}

func (setup *OrgSetup) UpdateBuyerRequest(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received Approve Buy request")
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	var request requestBody
	err = json.Unmarshal(body, &request)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	requestNotFound := true
	for i, BuyerReq := range buyerRequests {
		fmt.Println("buyer request ")
		if BuyerReq.RequestId == request.RequestId && BuyerReq.OwnerId == request.OwnerId {
			requestNotFound = false
			fmt.Sprintf("request and owner id matched")
			if strings.ToUpper(request.Status) == "APPROVED" {

				chainCodeName := constants.ChaincodeName
				channelID := "mychannel"
				function := "CreatePrivateRequest"
				//requestID string, sellerid string, buyerid string, financeRequired bool, landid string
				args := []string{
					BuyerReq.RequestId,
					BuyerReq.OwnerId,
					BuyerReq.BuyerId,
					BuyerReq.RequireLoan,
					BuyerReq.LandId,
				}

				fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %v\n", channelID, chainCodeName, function, args)

				network := setup.Gateway.GetNetwork(channelID)
				contract := network.GetContract(chainCodeName)

				txnProposal, err := contract.NewProposal(function, client.WithArguments(args...))
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					resp := Response{Status: err.Error()}
					bytes, _ := json.Marshal(resp)
					w.Write(bytes)
					return
				}

				txnEndorsed, err := txnProposal.Endorse()
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					resp := Response{Status: err.Error()}
					bytes, _ := json.Marshal(resp)
					w.Write(bytes)
					return
				}

				_, err = txnEndorsed.Submit()
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					resp := Response{Status: err.Error()}
					bytes, _ := json.Marshal(resp)
					w.Write(bytes)
					return
				}
				fmt.Printf("result: %v", txnEndorsed.Result())

				w.WriteHeader(http.StatusOK)
				var resp Response
				if strings.ToUpper(BuyerReq.RequireLoan) == "YES" {
					resp = Response{Status: "Processing At Bank"}
					request.Status = "Processing at Bank"
				} else {
					resp = Response{Status: constants.SUCCESS}
				}
				fmt.Printf("resp: %v", resp)

				bytes, _ := json.Marshal(resp)
				fmt.Printf("Bytes: %s", bytes)

				w.Write(bytes)

			}
			buyerRequests[i].Status = request.Status
			//fmt.Fprintf(w, "Updated Buyer's Request !!")
			return
		}
	}
	if requestNotFound {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: "Cannot Find Request"}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
	}
	return
}

func (setup *OrgSetup) SellerRaiseRequest(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received Approve Buy request")
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	var request requestBody
	err = json.Unmarshal(body, &request)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	request.RequestId = uuid.New().String()
	if strings.ToUpper(request.Status) == "APPROVED" {

		chainCodeName := constants.ChaincodeName
		channelID := "mychannel"
		function := "CreatePrivateRequest"
		//requestID string, sellerid string, buyerid string, financeRequired bool, landid string
		args := []string{
			request.RequestId,
			request.OwnerId,
			request.BuyerId,
			request.RequireLoan,
			request.LandId,
		}

		fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %v\n", channelID, chainCodeName, function, args)

		network := setup.Gateway.GetNetwork(channelID)
		contract := network.GetContract(chainCodeName)

		txnProposal, err := contract.NewProposal(function, client.WithArguments(args...))
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			resp := Response{Status: err.Error()}
			bytes, _ := json.Marshal(resp)
			w.Write(bytes)
			return
		}

		txnEndorsed, err := txnProposal.Endorse()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			resp := Response{Status: err.Error()}
			bytes, _ := json.Marshal(resp)
			w.Write(bytes)
			return
		}

		_, err = txnEndorsed.Submit()
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			resp := Response{Status: err.Error()}
			bytes, _ := json.Marshal(resp)
			w.Write(bytes)
			return
		}
		fmt.Printf("result: %v", txnEndorsed.Result())

		w.WriteHeader(http.StatusOK)
		resp := Response{Status: constants.SUCCESS}
		fmt.Printf("resp: %v", resp)

		bytes, _ := json.Marshal(resp)
		fmt.Printf("Bytes: %s", bytes)

		w.Write(bytes)

	}
	//fmt.Fprintf(w, "Updated Buyer's Request !!")
	return

}

func (setup *OrgSetup) FetchRequestsByUserId(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received FetchUser request")
	queryParams := r.URL.Query()
	fmt.Println(queryParams)
	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "GetAllPrivateRequestsByUserId"
	args := queryParams.Get("userid")
	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %s\n", channelID, chainCodeName, function, args)
	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)
	evaluateResponse, err := contract.EvaluateTransaction(function, args)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(evaluateResponse)
}

func (setup *OrgSetup) FetchRequestById(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received FetchUser request")
	queryParams := r.URL.Query()
	fmt.Println(queryParams)
	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "ReadPrivateRequest"
	args := queryParams.Get("requestid")
	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %s\n", channelID, chainCodeName, function, args)
	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)
	evaluateResponse, err := contract.EvaluateTransaction(function, args)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(evaluateResponse)
}

func (setup *OrgSetup) RegisterUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received Invoke request")

	// Read the request body
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	var user User
	err = json.Unmarshal(body, &user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "CreatePrivateUser"
	//name string, userType string, email string, aadhar string, password string, address string
	args := []string{
		user.Name,
		user.Type,
		user.Email,
		user.Aadhar,
		user.Password,
		user.Address,
	}

	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %v\n", channelID, chainCodeName, function, args)

	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)

	txnProposal, err := contract.NewProposal(function, client.WithArguments(args...))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	txnEndorsed, err := txnProposal.Endorse()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	_, err = txnEndorsed.Submit()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	if txnEndorsed.Result() == nil {
		w.WriteHeader(http.StatusOK)
		r := Response{Status: constants.SUCCESS}
		bytes, _ := json.Marshal(r)
		w.Write(bytes)
	}
}

func (setup *OrgSetup) FetchUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received FetchUser request")
	queryParams := r.URL.Query()
	fmt.Println(queryParams)
	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "ReadPrivateUser"
	args := queryParams.Get("aadhar")
	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %s\n", channelID, chainCodeName, function, args)
	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)
	evaluateResponse, err := contract.EvaluateTransaction(function, args)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(evaluateResponse)
}

func (setup *OrgSetup) RegisterLand(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received Invoke request")

	// Read the request body
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	var land Land
	err = json.Unmarshal(body, &land)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	land.LandId = uuid.New().String()
	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "CreatePrivLandAsset"
	//landID string, OwnerId string, area string, price string, location string, address string
	args := []string{
		land.LandId,
		land.OwnerId,
		land.Area,
		land.Price,
		land.Location,
		land.Address,
	}

	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %v\n", channelID, chainCodeName, function, args)

	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)

	txnProposal, err := contract.NewProposal(function, client.WithArguments(args...))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	txnEndorsed, err := txnProposal.Endorse()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	_, err = txnEndorsed.Submit()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	if txnEndorsed.Result() == nil {
		w.WriteHeader(http.StatusOK)
		resp := Response{Status: constants.SUCCESS}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
	}
}

func (setup *OrgSetup) FetchLandById(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received FetchUser request")
	queryParams := r.URL.Query()
	fmt.Println(queryParams)
	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "ReadPrivLandAsset"
	args := queryParams.Get("landid")
	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %s\n", channelID, chainCodeName, function, args)
	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)
	evaluateResponse, err := contract.EvaluateTransaction(function, args)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(evaluateResponse)
}

func (setup *OrgSetup) FetchAllLands(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received FetchUser request")
	queryParams := r.URL.Query()
	fmt.Println(queryParams)
	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "GetAllPrivateLandAssets"
	fmt.Printf("channel: %s, chaincode: %s, function: %s \n", channelID, chainCodeName, function)
	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)
	evaluateResponse, err := contract.EvaluateTransaction(function)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(evaluateResponse)
}
