package main

import (
	"fmt"
	"rest-api-go/web"
)

func main() {
	//Initialize setup for Regulator
	cryptoPath := "/home/arun.padala@npci.org.in/Desktop/capstone/capstone_full_new/fabric-samples_old/test-network/organizations/peerOrganizations/regulator.landrecords.com"
	orgConfig := web.OrgSetup{
		OrgName:      "regulator",
		MSPID:        "RegulatorMSP",
		CertPath:     cryptoPath + "/users/User1@regulator.landrecords.com/msp/signcerts/User1@regulator.landrecords.com-cert.pem",
		KeyPath:      cryptoPath + "/users/User1@regulator.landrecords.com/msp/keystore/",
		TLSCertPath:  cryptoPath + "/peers/peer0.regulator.landrecords.com/tls/ca.crt",
		PeerEndpoint: "localhost:9051",
		GatewayPeer:  "peer0.regulator.landrecords.com",
	}

	orgSetup, err := web.Initialize(orgConfig)
	if err != nil {
		fmt.Println("Error initializing setup for Regulator: ", err)
	}
	web.Serve(web.OrgSetup(*orgSetup))
}
