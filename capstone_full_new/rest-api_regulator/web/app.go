package web

import (
	"fmt"
	"net/http"

	"github.com/hyperledger/fabric-gateway/pkg/client"
)

// OrgSetup contains organization's config to interact with the network.
type OrgSetup struct {
	OrgName      string
	MSPID        string
	CryptoPath   string
	CertPath     string
	KeyPath      string
	TLSCertPath  string
	PeerEndpoint string
	GatewayPeer  string
	Gateway      client.Gateway
}

// Serve starts http web server.
func Serve(setups OrgSetup) {
	mux := http.NewServeMux()

	// Attach CORS middleware
	handler := corsMiddleware(mux)

	mux.HandleFunc("/get-land-by-id", setups.FetchLandById)
	mux.HandleFunc("/get-all-lands", setups.FetchAllLands)
	mux.HandleFunc("/approve-land", setups.ApproveLandAsset)
	//mux.HandleFunc("/get-request-by-id", setups.FetchRequest)
	//mux.HandleFunc("/get-all-requests", setups.FetchAllRequests)
	//mux.HandleFunc("/update-request", setups.UpdateRequest)
	fmt.Println("Listening (http://localhost:3000/)...")
	if err := http.ListenAndServe(":3000", handler); err != nil {
		fmt.Println(err)
	}
}

func corsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")

		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusNoContent)
			return
		}

		next.ServeHTTP(w, r)
	})
}
