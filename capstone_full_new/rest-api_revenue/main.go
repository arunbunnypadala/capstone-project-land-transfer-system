package main

import (
	"fmt"
	"rest-api-go/web"
)

func main() {
	//Initialize setup for Revenue
	cryptoPath := "/home/arun.padala@npci.org.in/Desktop/capstone/capstone_full_new/fabric-samples_old/test-network/organizations/peerOrganizations/revenue.landrecords.com"
	orgConfig := web.OrgSetup{
		OrgName:      "revenue",
		MSPID:        "RevenueMSP",
		CertPath:     cryptoPath + "/users/User1@revenue.landrecords.com/msp/signcerts/User1@revenue.landrecords.com-cert.pem",
		KeyPath:      cryptoPath + "/users/User1@revenue.landrecords.com/msp/keystore/",
		TLSCertPath:  cryptoPath + "/peers/peer0.revenue.landrecords.com/tls/ca.crt",
		PeerEndpoint: "localhost:11051",
		GatewayPeer:  "peer0.revenue.landrecords.com",
	}

	orgSetup, err := web.Initialize(orgConfig)
	if err != nil {
		fmt.Println("Error initializing setup for Revenue: ", err)
	}
	web.Serve(web.OrgSetup(*orgSetup))
}
