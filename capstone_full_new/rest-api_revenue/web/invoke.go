package web

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"rest-api-go/constants"

	"github.com/hyperledger/fabric-gateway/pkg/client"
)

type Request struct {
	RequestId        string `json:"requestid"`
	SellerId         string `json:"sellerid"`
	BuyerId          string `json:"buyerid"`
	Finance_Required bool   `json:"finance_required"`
	LandId           string `json:"landid"`
	Registrar_Status string `json:"registrar_Status"`
	Revenue_Status   string `json:"revenue_Status"`
	BankLoan_Status  string `json:"bankloan_Status"`
}
type Response struct {
	Status string `json:"Status"`
}
type Land struct {
	LandId           string `json:"landid"`
	OwnerId          string `json:"ownerid"`
	Area             string `json:"area"`
	Price            string `json:"price"`
	Location         string `json:"location"` //coordinates
	Address          string `json:"address"`
	Registrar_Status string `json:"registrar_Status"`
	Revenue_Status   string `json:"revenue_Status"`
}

func (setup *OrgSetup) FetchLandById(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received FetchUser request")
	queryParams := r.URL.Query()
	fmt.Println(queryParams)
	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "ReadPrivLandAsset"
	args := queryParams.Get("landid")
	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %s\n", channelID, chainCodeName, function, args)
	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)
	evaluateResponse, err := contract.EvaluateTransaction(function, args)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(evaluateResponse)
}

func (setup *OrgSetup) FetchAllLands(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received FetchUser request")
	queryParams := r.URL.Query()
	fmt.Println(queryParams)
	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "GetAllPrivateLandAssets"
	fmt.Printf("channel: %s, chaincode: %s, function: %s \n", channelID, chainCodeName, function)
	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)
	evaluateResponse, err := contract.EvaluateTransaction(function)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(evaluateResponse)
}

func (setup *OrgSetup) ApproveLandAsset(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Received Invoke request")

	// Read the request body
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	var land Land
	err = json.Unmarshal(body, &land)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	chainCodeName := constants.ChaincodeName
	channelID := "mychannel"
	function := "ApprovePrivLandAsset"
	//landID string, status string
	args := []string{
		land.LandId,
		land.Revenue_Status,
	}

	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %v\n", channelID, chainCodeName, function, args)

	network := setup.Gateway.GetNetwork(channelID)
	contract := network.GetContract(chainCodeName)

	txnProposal, err := contract.NewProposal(function, client.WithArguments(args...))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	txnEndorsed, err := txnProposal.Endorse()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}

	_, err = txnEndorsed.Submit()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		resp := Response{Status: err.Error()}
		bytes, _ := json.Marshal(resp)
		w.Write(bytes)
		return
	}
	if txnEndorsed.Result() == nil {
		w.WriteHeader(http.StatusOK)
		r := Response{Status: constants.SUCCESS}
		bytes, _ := json.Marshal(r)
		w.Write(bytes)
	}
}

//func (setup *OrgSetup) FetchRequest(w http.ResponseWriter, r *http.Request) {
//	fmt.Println("Received FetchBuyerRequest request")
//	queryParams := r.URL.Query()
//	fmt.Println(queryParams)
//	chainCodeName := constants.ChaincodeName
//	channelID := "mychannel"
//	function := "ReadPrivateRequest"
//	args := queryParams.Get("requestID")
//	fmt.Printf("channel: %s, chaincode: %s, function: %s, args: %s\n", channelID, chainCodeName, function, args)
//	network := setup.Gateway.GetNetwork(channelID)
//	contract := network.GetContract(chainCodeName)
//	evaluateResponse, err := contract.EvaluateTransaction(function, args)
//	if err != nil {
//		w.WriteHeader(http.StatusInternalServerError)
//		resp := Response{Status: err.Error()}
//		bytes, _ := json.Marshal(resp)
//		w.Write(bytes)
//		return
//	}
//	w.WriteHeader(http.StatusOK)
//	w.Write(evaluateResponse)
//}
//
//func (setup *OrgSetup) FetchAllRequests(w http.ResponseWriter, r *http.Request) {
//	fmt.Println("Received FetchAllBuyerRequests request")
//	queryParams := r.URL.Query()
//	fmt.Println(queryParams)
//	chainCodeName := constants.ChaincodeName
//	channelID := "mychannel"
//	function := "GetAllPrivateRequests"
//	fmt.Printf("channel: %s, chaincode: %s, function: %s \n", channelID, chainCodeName, function)
//	network := setup.Gateway.GetNetwork(channelID)
//	contract := network.GetContract(chainCodeName)
//	evaluateResponse, err := contract.EvaluateTransaction(function)
//	if err != nil {
//		w.WriteHeader(http.StatusInternalServerError)
//		resp := Response{Status: err.Error()}
//		bytes, _ := json.Marshal(resp)
//		w.Write(bytes)
//		return
//	}
//	w.WriteHeader(http.StatusOK)
//	w.Write(evaluateResponse)
//}
//
//
//
//
//
//func (setup *OrgSetup) UpdateRequest(w http.ResponseWriter, r *http.Request) {
//	fmt.Println("Received UpdateRequest request")
//	chainCodeName := constants.ChaincodeName
//	channelID := "mychannel"
//	function := "UpdatePrivateRequest"
//
//	// Read the request body
//	body, err := io.ReadAll(r.Body)
//	if err != nil {
//		w.WriteHeader(http.StatusInternalServerError)
//		resp := Response{Status: err.Error()}
//		bytes, _ := json.Marshal(resp)
//		w.Write(bytes)
//		return
//	}
//
//	var request Request
//	err = json.Unmarshal(body, &request)
//	if err != nil {
//		w.WriteHeader(http.StatusInternalServerError)
//		resp := Response{Status: err.Error()}
//		bytes, _ := json.Marshal(resp)
//		w.Write(bytes)
//		return
//	}
//
//	args := []string{
//		request.RequestId,
//		request.Revenue_Status,
//	}
//	fmt.Printf("request : %v", request)
//
//	network := setup.Gateway.GetNetwork(channelID)
//	contract := network.GetContract(chainCodeName)
//
//	txnProposal, err := contract.NewProposal(function, client.WithArguments(args...))
//	if err != nil {
//		w.WriteHeader(http.StatusInternalServerError)
//		resp := Response{Status: err.Error()}
//		bytes, _ := json.Marshal(resp)
//		w.Write(bytes)
//		return
//	}
//
//	txnEndorsed, err := txnProposal.Endorse()
//	if err != nil {
//		w.WriteHeader(http.StatusInternalServerError)
//		resp := Response{Status: err.Error()}
//		bytes, _ := json.Marshal(resp)
//		w.Write(bytes)
//		return
//	}
//
//	_, err = txnEndorsed.Submit()
//	if err != nil {
//		w.WriteHeader(http.StatusInternalServerError)
//		resp := Response{Status: err.Error()}
//		bytes, _ := json.Marshal(resp)
//		w.Write(bytes)
//		return
//	}
//
//	if txnEndorsed.Result() == nil {
//		w.WriteHeader(http.StatusOK)
//		r := Response{Status: constants.SUCCESS}
//		bytes, _ := json.Marshal(r)
//		w.Write(bytes)
//	}
//
//}
